package com.company;

import java.util.Arrays;
import java.util.Random;
import java.util.Scanner;

public class Main {

    public static <hiddenNumber> void main(String[] args) {
        String[][] arr = {{"When ceasefire is  signed between Azerbaijan and Armenia? ", "When did the World War II end? ", "When did the World War II begin? ",
                "When was 4th suicide attack at the World Center? ", "When did the Soviet Army massacre Baku? ", "When Azerbaijani people loose Khojali? ",
                "When did the World War I begin? ", "When was Azerbaijan Democratic Republic established? ", "When was the Treaty of Turkmenchay signed? ",
                "When did the World War I end? "},
                {"2020", "1945", "1941", "2001", "1990", "1992", "1914", "1918", "1828", "1918"}};
        Random random = new Random();
        int hiddenNumber = random.nextInt(arr[0].length);
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter your name: ");
        String name = scanner.next();
        int[] value = new int[arr[0].length];
        int num;
        String str;
        System.out.println("Let the game begin!");
        int i = 0;
        int[] a = new int[0];
        System.out.println(arr[0][hiddenNumber]);
        for (int m = 0; m < arr[0].length; m++) {
            value[m] = Integer.parseInt(arr[1][m]);
        }
        while (true) {
            a = Arrays.copyOf(a, i + 1);
            try {
                str = scanner.next();
                num = Integer.parseInt(str);
                a[i] = num;
                i++;
                if (num > value[hiddenNumber]) System.out.println("Your number is too big. Please, try again.");
                else if (num < value[hiddenNumber])
                    System.out.println("Your number is too small. Please, try again.");
                else {
                    System.out.println("Congratulations, " +name +"!");
                    Arrays.sort(a);
                    System.out.print("Your numbers:");
                    for (int k = a.length - 1; k >= 0; k--) {
                        System.out.print(" " + a[k]);
                    }
                    break;
                }
            } catch (NumberFormatException e) {
                System.out.println("Please enter a number!");
            }
        }
    }
}
