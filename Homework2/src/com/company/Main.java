package com.company;

import java.util.Random;
import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        System.out.println("All set. Get ready to rumble!");
        Scanner scanner = new Scanner(System.in);
        String[][] arr = new String[6][6];
        Random random = new Random();
        int hiddenRow = random.nextInt(3) + 2;
        int hiddenColumn = random.nextInt(3) + 2;
        int value1, value2;
        int position = random.nextInt(2);
        for (int i = 0; i < arr[0].length; i++) {
            for (int j = 0; j < arr[0].length; j++) {
                if (i == 0) {
                    arr[i][j] = String.valueOf(j);
                } else if (j == 0) arr[i][j] = String.valueOf(i);
                else arr[i][j] = "-";
            }
        }
        while (true) {
            int count = 0;
            for (int i = 0; i < arr[0].length; i++) {
                for (int j = 0; j < arr[0].length; j++) {
                    System.out.print(arr[i][j] + " | ");
                    if (arr[i][j] == "x") {
                        count = count + 1;
                    }
                }
                System.out.println("");
            }
            if (count == 3) {
                System.out.println("You have won!");
                break;
            }
            System.out.print("Enter numbers for row and column: ");
            String row = scanner.next();
            String column = scanner.next();
            try {
                value1 = Integer.parseInt(row);
                value2 = Integer.parseInt(column);
                if (value1 >= 1 && value1 <= 5 && value2 >= 1 && value2 <= 5) {
                    if (value1 == hiddenRow && value2 == hiddenColumn) {
                        arr[value1][value2] = "x";
                        continue;
                    }
                    if (position == 1) {
                        if (value1 == hiddenRow - 1 && value2 == hiddenColumn) {
                            arr[value1][value2] = "x";
                            continue;
                        } else if (value1 == hiddenRow + 1 && value2 == hiddenColumn) {
                            arr[value1][value2] = "x";
                            continue;
                        }
                    } else {
                        if (value1 == hiddenRow && value2 == hiddenColumn - 1) {
                            arr[value1][value2] = "x";
                            continue;
                        } else if (value1 == hiddenRow && value2 == hiddenColumn + 1) {
                            arr[value1][value2] = "x";
                            continue;
                        }
                    }
                    arr[value1][value2] = "*";
                } else System.out.println("Please enter a number between 1 and 5!");
            } catch (NumberFormatException e) {
                System.out.println("Please enter a number!");
            }
        }
    }
}
